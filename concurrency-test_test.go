package concurrencytest_test

import (
	"testing"
	"time"

	concurrencytest "github.com/cfabrica46/concurrency-test"
)

func mockUserChecker(user string) (check bool) {
	return user != "yo"
}

func TestGetUsersValidated(t *testing.T) {
	t.Parallel()

	for _, tt := range []struct {
		name  string
		out   map[string]bool
		users []string
	}{
		{
			name: "NoError",
			users: []string{
				"cesar",
				"arturo",
				"yo",
			},
			out: map[string]bool{
				"cesar":  true,
				"arturo": true,
				"yo":     false,
			},
		},
	} {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			result := concurrencytest.GetUsersValidated(mockUserChecker, tt.users)

			for userName := range tt.out {
				if tt.out[userName] != result[userName] {
					t.Errorf("want %v; got %v\n", tt.out[userName], result[userName])
				}
			}
		})
	}
}

func BenchmarkGetUsersValidated(b *testing.B) {
	urls := make([]string, 100)

	for i := 0; i < len(urls); i++ {
		urls[i] = "an user"
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		concurrencytest.GetUsersValidated(func(_ string) bool {
			time.Sleep(20 * time.Millisecond)

			return true
		}, urls)
	}
}
