package concurrencytest

type UsersChecker func(string) bool

type myResult struct {
	user  string
	value bool
}

func GetUsersValidated(uc UsersChecker, users []string) (result map[string]bool) {
	result = make(map[string]bool)

	resultChannel := make(chan myResult)

	for _, user := range users {
		go aux(user, resultChannel, uc)
	}

	for i := 0; i < len(users); i++ {
		r := <-resultChannel
		result[r.user] = r.value
	}

	return result
}

func aux(u string, resultChannel chan myResult, uc UsersChecker) {
	resultChannel <- myResult{
		user:  u,
		value: uc(u),
	}
}
